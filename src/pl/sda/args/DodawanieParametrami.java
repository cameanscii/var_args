package pl.sda.args;

public class DodawanieParametrami {
    public static void main(String[] args) {

        int[] liczby = zamienNaLiczby(args);


        int cyfra = 0;
        for (int liczba : liczby) {
            cyfra += liczba;
        }


        System.out.println("Suma to: " + cyfra);


    }

    public static int[] zamienNaLiczby(String[] args) {

        int[] liczby = new int[args.length];

        for (int i = 0; i < liczby.length; i++) {
            liczby[i] = Integer.parseInt(args[i]);
        }

        return liczby;

    }
}
