package pl.sda.kalkulacje;

import pl.sda.args.DodawanieParametrami;

public class Kalkulator {

    public static void main(String[] args) {

        String dzialanie = args[0];// +,-
        args[0] = "0";
        Kalkulator calc = new Kalkulator();
        int[] liczby = DodawanieParametrami.zamienNaLiczby(args);
        switch (dzialanie) {
            case "+":
                System.out.println(calc.dodaj(liczby));
                break;
            case "-":
                System.out.println(calc.odejmij(liczby));
                break;
        }
    }

    public int dodaj(int... liczby) {
        int suma = 0;
        for (int liczba : liczby) {
            suma += liczba;

        }
        return suma;

    }

    public int odejmij(int... liczby) {
        int roznica = liczby[1];
        for (int i = 2; i < liczby.length; i++) {
            roznica -= liczby[i];
        }
        return roznica;
    }


}

